name := "twitter-user-statistic"

organization := "pl.jborkowski"

version := "0.0.1"

scalaVersion := "2.11.8"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  ws,
  specs2,
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.0.play24"
)

routesGenerator := InjectedRoutesGenerator

com.typesafe.sbt.SbtScalariform.scalariformSettings