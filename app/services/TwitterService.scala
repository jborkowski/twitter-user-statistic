package services

import play.api.Play
import play.api.libs.oauth.{ConsumerKey, OAuthCalculator, RequestToken}
import play.api.libs.ws.WS

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

trait TwitterService {
  def fetchRelationshipsTweets(userName: String) (implicit ec: ExecutionContext): Future[TwitterCounts]
  def postTweet(message: String) (implicit ec: ExecutionContext): Future[Unit]
}

case class TwitterCounts(followersCounts: Long, friendsCounts: Long)

class TwitterServiceImp extends TwitterService {
  override def fetchRelationshipsTweets(userName: String)(implicit ec: ExecutionContext): Future[TwitterCounts] = {
    val url = "https://api.twitter.com/1.1/users/show.json"
    credentials map {
      case (consumerKey, requestToken) =>
        WS
          .url(url)
          .sign(OAuthCalculator(consumerKey, requestToken))
          .withQueryString("screen_name" -> userName)
          .get().map{ response =>
            if (response.status == 200) {
              TwitterCounts(
                (response.json \ "followers_count").as[Long],
                (response.json \ "friends_count").as[Long]
              )
            } else {
              throw TwitterServiceException(s"I could not retrieve counts for $userName")
            }
          }
    } getOrElse {
      Future.failed(TwitterServiceException("You did not correctly configure Twitter conditionals"))
    }
  }

  override def postTweet(message: String)(implicit ec: ExecutionContext): Future[Unit] = ???

  private def credentials: Option[(ConsumerKey, RequestToken)] = for {
    apiKey <- Play.configuration.getString("twitter.apiKey")
    apiSecret <- Play.configuration.getString("twitter.apiSecret")
    token <- Play.configuration.getString("twitter.token")
    tokenSecret <- Play.configuration.getString("twitter.tokenSecret")
  } yield (
    ConsumerKey(apiKey, apiSecret),
    RequestToken(token, tokenSecret)
    )
}

case class TwitterServiceException(msg: String) extends RuntimeException
