package services

import javax.inject.Inject

import org.joda.time.DateTime
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

trait StatisticsRepository {
  def storeCounts(counts: StoredCounts) (implicit ec: ExecutionContext): Future[Unit]
  def retrieveLatestCounts(userName: String) (implicit ec: ExecutionContext): Future[StoredCounts]
}

class MongoStatisticRepository @Inject() (reactiveMongo: ReactiveMongoApi) extends StatisticsRepository {
  private val StatisticsCollection = "UserStatistics"

  private lazy val collection = reactiveMongo.db.collection[BSONCollection](StatisticsCollection)

  override def storeCounts(counts: StoredCounts)(implicit ec: ExecutionContext): Future[Unit] = {
    collection.insert(counts).map { lastError =>
      if(lastError.inError) {
        throw CountStorageException(counts)
      }
    }
  }

  override def retrieveLatestCounts(userName: String)(implicit ec: ExecutionContext): Future[StoredCounts] = {
    val query = BSONDocument("userName" -> userName)
    val order = BSONDocument("_id" -> -1)
    collection
      .find(query)
      .sort(order)
      .one[StoredCounts]
      .map { counts =>
        counts getOrElse StoredCounts(DateTime.now, userName, 0, 0)
      } recover {
        case NonFatal(t) =>
          throw CountRetrievalException(userName, t)
      }
  }
}

case class StoredCounts(when: DateTime, userName: String, followersCount: Long, friendsCount: Long)
case class CountRetrievalException(userName: String, cause: Throwable) extends RuntimeException("Could not read counts for " + userName)
case class CountStorageException(counts: StoredCounts) extends RuntimeException