package services

import java.time.Period

import org.joda.time.DateTime
import sun.awt.CausedFocusEvent.Cause

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

trait StatisticService {
  def createUserService(userName: String) (implicit ec: ExecutionContext): Future[Unit]
}

class DefaultStatisticsService(statisticsRepository: StatisticsRepository, statisticService: TwitterService)
  extends StatisticService {
  override def createUserService(userName: String)(implicit ec: ExecutionContext): Future[Unit] = {

    def storeCounts(counts: (StoredCounts, TwitterCounts)): Future[Unit] = {
      counts match { case (prev, curr) =>
        statisticsRepository.storeCounts(StoredCounts(
          DateTime.now,
          userName,
          curr.followersCounts,
          curr.friendsCounts
        ))
      }
    }

    def publishMsg(counts: (StoredCounts, TwitterCounts)): Future[Unit] = {
      Future[Unit] = counts match { case (prev, curr) =>
          val followersDiff = curr.followersCounts - prev.followersCount
          val friendDiff = curr.friendsCounts - prev.friendsCount
          def phrasing(difference: Long) = if (difference > 0) "gained" else "lost"
          val durationInDays = new Period(prev.when, DateTime.now)

        statisticService.postTweet(
          s"@$userName in the past $durationInDays you have" +
          s" ${phrasing(followersDiff)} $followersDiff followers and " +
          s"${phrasing(followersDiff)} $friendDiff friends"
        )
      }
    }

    val previousCounts: Future[StoredCounts] = statisticsRepository.retrieveLatestCounts(userName)
    val currentCounts: Future[TwitterCounts] = statisticService.fetchRelationshipsTweets(userName)

    val counts: Future[(StoredCounts, TwitterCounts)] = for {
      previous <- previousCounts
      current <- currentCounts
    } yield {
      (previous, current)
    }

    val storedCounts: Future[Unit] = counts.flatMap(storeCounts)
    val publishedMsg: Future[Unit] = counts.flatMap(publishMsg)

    val result = for {
      _ <- storedCounts
      _ <- publishedMsg
    } yield {}

    result recoverWith {
      case CountStorageException(countsToStorage) =>
        retryStoring(countsToStorage, attemptNumber = 0)
    } recover {
      case CountStorageException(countsToStorage) =>
        throw StatisticsServiceFailed("We couldn't save the statistics to our database. Next time it will work!")
      case CountRetrievalException(user, cause) =>
        throw StatisticsServiceFailed("We have a problem with our Database, Sorry!", cause)
      case TwitterServiceException(msg) =>
        throw StatisticsServiceFailed(s"We have a problem contacting with Twitter: $msg")
      case NonFatal(t) =>
        throw StatisticsServiceFailed("We have a unknown problem. Sorry!")
    }
  }


  private def retryStoring(counts: StoredCounts, attemptNumber: Int)(implicit ec: ExecutionContext): Future[Unit] = {
    if (attemptNumber < 3) {
      statisticsRepository.storeCounts(counts).recoverWith {
        case NonFatal(t) => retryStoring(counts, attemptNumber + 1)
      }
    } else {
      Future.failed(CountStorageException(counts))
    }
  }
}

class StatisticsServiceFailed(cause: Throwable) extends RuntimeException(cause) {
  def this(message: String) = this(new RuntimeException(message))
  def this(message: String, cause: Throwable) = this(new RuntimeException(message, cause))
}

object StatisticsServiceFailed {
  def apply(message: String): StatisticsServiceFailed = new StatisticsServiceFailed(message)
  def apply(message: String, cause: Throwable): StatisticsServiceFailed = new StatisticsServiceFailed(message, cause)
}
